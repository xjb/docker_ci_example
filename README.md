# docker_ci_example

## Registering runners

```bash
$ docker run --rm -it -v gitlab_com-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register
Enter the GitLab instance URL (for example, https://gitlab.com/):
https://gitlab.com/
Enter the registration token:
Ug5GWPcJSZpYs_mR7SW4
Enter a description for the runner:
[89418878165b]: 
nter tags for the runner (comma-separated):

Registering runner... succeeded                     runner=Ug5GWPcJ
Enter an executor: docker-ssh+machine, kubernetes, custom, docker, docker-ssh, parallels, ssh, virtualbox, shell, docker+machine:
docker
Enter the default Docker image (for example, ruby:2.6):
alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 
```

## Run GitLab Runner

```bash
$ docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab_com-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest
```

## Configure TLS

```bash
$ docker run --rm -it -v gitlab_com-runner-config:/etc/gitlab-runner busybox vi /etc/gitlab-runner/config.toml
```

```diff
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
-     privileged = false
+     privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
```
